FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar

ENV USER_NAME developer
ENV APP_HOME /home/$USER_NAME/app

RUN addgroup -S devGroup && adduser -S developer -G devGroup
RUN mkdir $APP_HOME

USER $USER_NAME
WORKDIR $APP_HOME

ENTRYPOINT ["java", "-Dspring.profiles.active=prod", "-jar","/app.jar"]
